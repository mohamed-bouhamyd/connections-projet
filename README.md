**Pour builder et tester l'application**

1- Cloner le projet

git clone git clone git@bitbucket.org:mohamed-bouhamyd/connections-projet.git

2- Se déplacer dans le répértoire du projet cloné

cd connections-projet

3- Génération du package (jar)

mvn package

4- Lancer l'application

java -jar target/connections-0.0.1-SNAPSHOT.jar

Si vous voulez lancer l'application avec un fichier de configuration externe (ex externConfication.yml)
java -jar target/connections-0.0.1-SNAPSHOT.jar --spring.config.location=externConfication.yml

Les paramétres de l'applciation sont:

monitoring.jobExecutionEvery: 360000        //l'application sera exécuter chaque 1h

monitoring.file.path: "ConnectionLogs.txt"  // le chemin du fichier des connexions 

monitoring.to: "s1"                         // la liste des serveurs qui ont été connectés à un serveur donné durant cette heure

monitoring.from: "s2"                       // la liste des serveurs auxquels un serveur donné s'est connecté durant cette heure