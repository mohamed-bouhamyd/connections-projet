package com.monitoring.connexion.services;

import com.monitoring.connexion.configuration.ConnectionException;
import com.monitoring.connexion.models.Connection;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ConnectionServiceTest {

    @Autowired
    ConnectionService connectionService;
    List<Connection> connections = new ArrayList<Connection>();

    @Before
    void init() throws FileNotFoundException {
        WriteInTheFileTest();
        Date dateNow = new Date ();
        dateNow.setTime(Long.valueOf(Instant.now().getEpochSecond())*1000);
        Date lessThanOneHour = new Date ();
        lessThanOneHour.setTime(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())*100);
        Date beforeThanOneHour = new Date ();
        beforeThanOneHour.setTime(Long.valueOf(Instant.now().minusSeconds(3660).getEpochSecond())*100);
        Connection connectionFirst = new Connection(lessThanOneHour, "s1","s2");
        Connection connectionSecond = new Connection(lessThanOneHour, "s1","s2");
        Connection connectionThree = new Connection(lessThanOneHour, "s2","s1");
        Connection connectionFour = new Connection(lessThanOneHour, "s3","s4");
        Connection connectionFive = new Connection(lessThanOneHour, "s1","s4");
        Connection connectionSix= new Connection(lessThanOneHour, "s2","s1");
        connections.add(connectionFirst);
        connections.add(connectionSecond);
        connections.add(connectionThree);
        connections.add(connectionFour);
        connections.add(connectionFive);
        connections.add(connectionSix);
    }

    void WriteInTheFileTest() throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("ConnectionLogsTest.txt");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3660).getEpochSecond())+" s1 s2");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s1 s2");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s1 s2");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s2 s1");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s3 s4");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s1 s4");
        writer.println(Long.valueOf(Instant.now().minusSeconds(3000).getEpochSecond())+" s2 s1");
        writer.close();
    }

    @Test
    void getAllConnections() throws ConnectionException {
        List<Connection> connectionsActuals = connectionService.getAllConnections();
        assertTrue(connectionsActuals.size()==7);
    }

    @Test
    void getConnectionForTheLastHour() throws ConnectionException {
        List<Connection> connectionsActuals = connectionService.getConnectionForTheLastHour(1);
        assertTrue(connectionsActuals.size()==6);
    }

    @Test
    void getServerCalledServerName() throws ConnectionException {
        List<Connection> connections = connectionService.getConnectionForTheLastHour(1);
        List<String> connectionsActuals = connectionService.getServerCalledServerName(connections, "s1");
        assertTrue(connectionsActuals.size()==2);
    }

    @Test
    void getServerCalledByServerName()  throws ConnectionException {
        List<Connection> connections = connectionService.getConnectionForTheLastHour(1);
        List<String> connectionsActuals = connectionService.getServerCalledByServerName(connections, "s1");
        assertTrue(connectionsActuals.size()==3);

    }

    @Test
    void getServerWithMaxRequests() throws ConnectionException {
        List<Connection> connections = connectionService.getConnectionForTheLastHour(1);
        String actual = connectionService.getServerWithMaxRequests(connections);
        String expected = "s1";
        assertEquals(actual,expected);

    }
}