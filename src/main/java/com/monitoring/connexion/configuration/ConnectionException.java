package com.monitoring.connexion.configuration;

public class ConnectionException extends Exception {

    private int errCode;

    public ConnectionException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
