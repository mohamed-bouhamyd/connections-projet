package com.monitoring.connexion.services;

import com.monitoring.connexion.configuration.ConnectionException;
import com.monitoring.connexion.models.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileService {

    private static final int NUMBER_ITEM_BY_LINE = 3;
    @Value("${monitoring.file.path}")
    private String logfilePath;


    public List<Connection> getAllConnections() throws ConnectionException {
        try {
            Stream stream = Files.lines(Paths.get(logfilePath));
            return (List<Connection>) stream.filter(s -> !s.toString().isEmpty()).map(line -> transformLogLineToConnectionObjet(line.toString()))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnectionException(500, "Erreur lors de la lecture du fichier:" + logfilePath);
        }
    }

    public List<Connection> getConnectionForTheLastHour(int startLine) throws ConnectionException {
        Date dateNow = new Date();
        dateNow.setTime(Long.valueOf(Instant.now().getEpochSecond()) * 1000);
        Date dateAfterAHour = new Date();
        dateAfterAHour.setTime(Long.valueOf(Instant.now().minusSeconds(3600).getEpochSecond()) * 1000);
        try {
            Stream stream = Files.lines(Paths.get(logfilePath)).skip(startLine-1);
            return (List<Connection>) stream.filter(s -> !s.toString().isEmpty())
                    .map(line -> transformLogLineToConnectionObjet(line.toString()))
                    .filter(connection -> CheckIfDateIsBetweenStartAndEndDate(((Connection) connection).getDate(),dateAfterAHour,dateNow))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnectionException(500, "Erreur lors de la lecture du fichier:" + logfilePath);
        }
    }

    Boolean CheckIfDateIsBetweenStartAndEndDate(Date date, Date start, Date end){
        return date.after(start) &&  (date.before(end) | date.equals(end));
    }

    public Connection transformLogLineToConnectionObjet(String logLineTxt) {
        Connection logLine = new Connection();
        String[] logItems = logLineTxt.split(" ");
        if (logItems.length != NUMBER_ITEM_BY_LINE) {
            return logLine;
        } else {
            Date date = new Date();
            date.setTime(Long.valueOf(logItems[0]) * 1000);
            logLine.setDate(date);
            logLine.setServerFrom(logItems[1]);
            logLine.setServerTo(logItems[2]);
        }
        return logLine;
    }

}
