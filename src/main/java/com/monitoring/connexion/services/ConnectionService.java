package com.monitoring.connexion.services;

import com.monitoring.connexion.configuration.ConnectionException;
import com.monitoring.connexion.models.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ConnectionService {

    @Autowired
    private FileService fileService;

    public List<Connection> getAllConnections() throws ConnectionException {
        return fileService.getAllConnections();
    }

    public List<Connection> getConnectionForTheLastHour(int startLine) throws ConnectionException {
        return fileService.getConnectionForTheLastHour(startLine);
    }

    //la liste des serveurs qui ont été connectés à un serveur donné
    public List<String> getServerCalledServerName(List<Connection> logs, String serverName){
        return logs.stream().filter(logLine -> logLine.getServerTo().equals(serverName))
                .map(logLine -> logLine.getServerFrom()).collect(Collectors.toList());
    }

    //la liste des serveurs auxquels un serveur donné s'est connecté
    public List<String> getServerCalledByServerName(List<Connection> logs, String ServerName){
        return logs.stream().filter(logLine -> logLine.getServerFrom().equals(ServerName))
                .map(logLine -> logLine.getServerTo()).collect(Collectors.toList());
    }

    //le serveur qui a généré le plus de connections
    public String getServerWithMaxRequests(List<Connection> connections){
        Map<String,Integer> connectionsMap = new HashMap<String,Integer>();
        connections.stream().forEach(logLine -> {
                    if(connectionsMap.get(logLine.getServerFrom())==null){
                        connectionsMap.put(logLine.getServerFrom(),getServerCalledByServerName(connections,logLine.getServerFrom()).size());
                    }
                }
        );
        return connectionsMap.entrySet().stream().max((entry1, entry2) -> entry1.getValue() >= entry2.getValue() ? 1 : -1).get().getKey();
    }

}
