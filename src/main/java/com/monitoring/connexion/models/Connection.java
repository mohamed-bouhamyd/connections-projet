package com.monitoring.connexion.models;

import java.util.Date;

public class Connection {

    private Date date;
    private String serverFrom;
    private String serverTo;

    public Connection() {

    }

    public Connection(String serverFrom, String serverTo) {
        this.serverFrom = serverFrom;
        this.serverTo = serverTo;
    }

    public Connection(Date date, String serverFrom, String serverTo) {
        this.date = date;
        this.serverFrom = serverFrom;
        this.serverTo = serverTo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getServerFrom() {
        return serverFrom;
    }

    public void setServerFrom(String serverFrom) {
        this.serverFrom = serverFrom;
    }

    public String getServerTo() {
        return serverTo;
    }

    public void setServerTo(String serverTo) {
        this.serverTo = serverTo;
    }

    @Override
    public String toString() {
        return "LogLine{" +
                "date=" + date +
                ", serverFrom='" + serverFrom + '\'' +
                ", serverTo='" + serverTo + '\'' +
                '}';
    }
}
