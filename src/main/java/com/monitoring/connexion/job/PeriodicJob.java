package com.monitoring.connexion.job;
import com.monitoring.connexion.configuration.ConnectionException;
import com.monitoring.connexion.models.Connection;
import com.monitoring.connexion.services.ConnectionService;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
public class PeriodicJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeriodicJob.class);
    private static int START_LINE = 1;
    private ConnectionService connectionService;
    @Value("${monitoring.to}")
    private String serverTo;

    @Value("${monitoring.from}")
    private String serverFrom;

    @Autowired
    public PeriodicJob(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Scheduled(fixedRateString = "${monitoring.jobExecutionEvery}" , initialDelay = 5000)
    public void reportCurrentTime() throws ConnectionException {
        LOGGER.info("Exécution du job de connexion");
        List<Connection> connections = connectionService.getConnectionForTheLastHour(START_LINE);
        LOGGER.info("il y avait {} connexions", connections.size());
        List<String> connectionTo = connectionService.getServerCalledServerName(connections, serverTo);
        LOGGER.info("il y avait {} connexions à {}", connectionTo.size(), serverTo);
        List<String> connectionFrom = connectionService.getServerCalledByServerName(connections, serverFrom);
        LOGGER.info("il y avait {} connexions de {}", connectionFrom.size(), serverFrom);
        if(connections.size()>0) {
            String serverWithMaxRequest = connectionService.getServerWithMaxRequests(connections);
            LOGGER.info("Le serveur avec le plus de requetes est : {}", serverWithMaxRequest);
        }else{
            LOGGER.info("il y avait connexion pendant la derniere heure");
        }
        START_LINE= START_LINE+connections.size();
        LOGGER.info("Fin de l'exécution du job de connexion");
    }
}
