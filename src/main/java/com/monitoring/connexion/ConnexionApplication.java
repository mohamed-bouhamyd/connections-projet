package com.monitoring.connexion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ConnexionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConnexionApplication.class, args);
	}
	/*
		@Bean
		public CommandLineRunner commandLineRunner(LogService logService) {
			return args -> {
			logService.init();
			};
		}
		*/

}
